/* citation: littleosbook.github.io */

/** fb_write_cell:
   *  Writes a character with the given foreground and background to position i
     *  in the framebuffer.
     *
     *  @param i  The location in the framebuffer
     *  @param c  The character
     *  @param fg The foreground color
     *  @param bg The background color
*/

typedef unsigned int uint32_t;
typedef unsigned int uint8_t;
uint8_t *fb = (uint8_t *) 0x000B8000;
typedef enum {
	black = 0,
	white = 15
}color_t;

int slength(char *msg){
        int length = 0;
        while(msg[length] != 0x0){
                length++;
        }
        return length++;
}

void fb_write_cell(uint32_t cell, uint8_t c, unsigned char fg, unsigned char bg)
{
        cell = cell * 2;
        fb[cell] = c;
        fb[cell + 1] = ((fg & 0x0F) << 4) | (bg & 0x0F);
}

int display(char *msg, int displayx){
	int charcount = slength(msg);
	int loc = (charcount * displayx);
	for(int i = 0; i < displayx; i++){
		for(int j = 0; j < charcount; j++){
			fb_write_cell(i+j, msg[i], white, black);
		}
	}
	return loc;
}

void write_message(char *message, unsigned int length)
{
	unsigned char white = 15;
	unsigned char black = 0;

	for(unsigned int i = 0; i < length; i++){
		fb_write_cell(i, message[i], white, black);
	}
}

void clear(){
	for(int i = 0; i < 80 * 25; i++){
		fb_write_cell(i, ' ', black, black);
	}
}
